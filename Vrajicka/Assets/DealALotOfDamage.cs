using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DealALotOfDamage : MonoBehaviour
{
    private PlayerActions actions;

    private void Start()
    {
        actions = new PlayerActions();
        actions.Enable();
        //actions.SpellCasting.CastSpell.performed += ctx => DealDamageToEverything(1000000);
        actions.Dev.InstaKill.performed += ctx => DealDamageToEverything(100000);
    }

    private void DealDamageToEverything(int amount)
    {
        var healths = GameObject.FindObjectsOfType<HealthBehaviour>();
        foreach (var health in healths)
        {
            health.TakeDamage(new Damage(amount));
        }
    }
}
