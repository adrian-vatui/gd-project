using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Leaderboard : MonoBehaviour
{
    [SerializeField] private GameObject datesGameObject;
    [SerializeField] private GameObject wavesGameObject;
    [SerializeField] private GameObject timesGameObject;


    private void Awake()
    {
        // waves, time, datetime
        List<Tuple<int, int, string>> scores = new List<Tuple<int, int, string>>();
        for (int i = 0; i < PlayerPrefs.GetInt("index", 0); i++)
            scores.Add(new Tuple<int, int, string>(PlayerPrefs.GetInt("HighestWave" + i),
                PlayerPrefs.GetInt("Time" + i), PlayerPrefs.GetString("DateTime" + i)));

        scores = new List<Tuple<int, int, string>>(
            scores.OrderByDescending(s => s.Item1).ThenByDescending(s => s.Item2));

        List<Text> dates = new List<Text>();
        List<Text> waves = new List<Text>();
        List<Text> times = new List<Text>();

        foreach (Transform date in datesGameObject.transform)
            dates.Add(date.GetComponent<Text>());

        foreach (Transform wave in wavesGameObject.transform)
            waves.Add(wave.GetComponent<Text>());

        foreach (Transform time in timesGameObject.transform)
            times.Add(time.GetComponent<Text>());

        for (int i = 0; i < dates.Count; i++)
        {
            dates[i].text = i < scores.Count ? scores[i].Item3 : "";
            waves[i].text = i < scores.Count ? scores[i].Item1.ToString() : "";
            times[i].text = i < scores.Count ? scores[i].Item2.ToString() : "";
        }
    }
}
