using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOnContact : MonoBehaviour
{
    public int amountOfDamage = 15;

    private bool dealtDamage = false;


    [SerializeField]
    private bool dissapearOnContact = false;
    private void OnEnable()
    {
        dealtDamage = false;
    }

    private void OnTriggerEnter(Collider other)
    {
       
        if (dealtDamage) return;

        if(dissapearOnContact && other.gameObject.tag == "Floor")
        {
            Destroy(gameObject);
        }


        if (other.gameObject.tag != "Player") return;

        var health = other.gameObject.GetComponent<HealthBehaviour>();

        if (health is null) return;

        health.TakeDamage(new Damage(amountOfDamage));

        dealtDamage = true;

        if(dissapearOnContact)
        {
            Destroy(gameObject);
        }
    }
}
