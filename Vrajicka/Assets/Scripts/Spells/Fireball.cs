using UnityEngine;

namespace Spells
{
    public class Fireball : Spell
    {
        public override void Cast(Vector3 initialPosition, Quaternion initialRotation)
        {
            this.SpellObject = Object.Instantiate(this.Prefab, initialPosition, initialRotation);
            this.SpellObject.GetComponent<Rigidbody>().AddForce(this.SpellObject.transform.forward * 1000);
        }
    }
}
