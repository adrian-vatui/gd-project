using UnityEngine;


namespace Spells
{
    public abstract class Spell
    {
        protected GameObject Prefab;
        protected GameObject SpellObject;

        public Spell()
        {
            this.Prefab = (GameObject) Resources.Load($"Prefabs/Spells/{this.GetType().Name}", typeof(GameObject));
        }

        public abstract void Cast(Vector3 initialPosition, Quaternion initialRotation);
    }
}
