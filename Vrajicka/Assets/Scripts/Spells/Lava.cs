using UnityEngine;

namespace Spells
{
    public class Lava : Spell
    {
        public override void Cast(Vector3 initialPosition, Quaternion initialRotation)
        {
            var other = GameObject.Find("Lava(Clone)");
            if (other != null)
                Object.Destroy(other);

            SpellObject = Object.Instantiate(Prefab, new Vector3(initialPosition.x, 0.5f, initialPosition.z),
                initialRotation);

            // since it spawns centered on the player, move it forward
            SpellObject.transform.position +=
                SpellObject.transform.forward * (SpellObject.transform.localScale.z / 2) * 1.3f;
        }
    }
}
