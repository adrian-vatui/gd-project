using Unity.VisualScripting;
using UnityEngine;

namespace Spells
{
    public class EarthballBehaviour : MonoBehaviour
    {
        [SerializeField] private int damage;
        private bool _isColliding;

        public void OnCollisionEnter(Collision collision)
        {
            if (_isColliding) return;
            _isColliding = true;

            // Debug.Log($"Hit {collision.gameObject.name}");
            if (collision.gameObject.CompareTag("Enemy"))
            {
                var health = collision.gameObject.GetComponentInParent<HealthBehaviour>();
                health.TakeDamage(new Damage(damage, Elements.Earth));
                if (health.currentHealth == 0)
                {
                    Destroy(collision.gameObject.GetComponent<StateMachine>());
                    var enemyRigidbody = collision.gameObject.GetComponent<Rigidbody>();
                    if (enemyRigidbody is null) return;
                    enemyRigidbody.isKinematic = false;
                    enemyRigidbody.AddForce(Quaternion.AngleAxis(180, Vector3.up) *
                                            (collision.impulse / Time.fixedDeltaTime));
                }
            }

            Destroy(gameObject);
            // spawn particle effects etc
        }

        private void Update()
        {
            _isColliding = false;
        }
    }
}
