using UnityEngine;

namespace Spells
{
    public class Steam : Spell
    {
        public override void Cast(Vector3 initialPosition, Quaternion initialRotation)
        {
            GameObject.Find("Player").GetComponent<Movement>().Dash();
        }
    }
}
