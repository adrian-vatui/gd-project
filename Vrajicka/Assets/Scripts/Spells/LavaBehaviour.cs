using System;
using Unity.VisualScripting;
using UnityEngine;
using Utils;

namespace Spells
{
    public class LavaBehaviour : MonoBehaviour
    {
        [SerializeField] private float destroyAfter = 5;

        void Start()
        {
            Destroy(gameObject, destroyAfter);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Enemy") && other.GetComponent<DamageOverTime>() == null)
                other.AddComponent<DamageOverTime>();
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject.CompareTag("Enemy") && other.GetComponent<DamageOverTime>() == null)
                other.AddComponent<DamageOverTime>();
        }
    }
}
