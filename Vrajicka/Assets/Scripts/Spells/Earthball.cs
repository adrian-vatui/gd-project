using UnityEngine;

namespace Spells
{
    public class Earthball : Spell
    {
        public override void Cast(Vector3 initialPosition, Quaternion initialRotation)
        {
            SpellObject = Object.Instantiate(Prefab, initialPosition, initialRotation);
            SpellObject.GetComponent<Rigidbody>().AddForce(Vector3.up * 100 + SpellObject.transform.forward * 2000);
        }
    }
}
