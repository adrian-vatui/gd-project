using System;
using System.Collections;
using System.Collections.Generic;
using Utils;

namespace Spells
{
    public class SpellFactory
    {
        private static Dictionary<List<Elements>, Spell> _elementsToSpell =
            new(new ListComparer<Elements>())
            {
                [new List<Elements>() {Elements.Fire, Elements.Fire}] = new Fireball(),
                [new List<Elements>() {Elements.Fire, Elements.Water}] = new Steam(),
                [new List<Elements>() {Elements.Fire, Elements.Earth}] = new Lava(),

                [new List<Elements>() {Elements.Water, Elements.Water}] = new Waterball(),
                [new List<Elements>() {Elements.Water, Elements.Earth}] = new Mud(),

                [new List<Elements>() {Elements.Earth, Elements.Earth}] = new Earthball(),
            };

        public static Spell CreateSpell(Elements[] elements)
        {
            var sortedElements = new List<Elements>(elements);
            sortedElements.Sort();

            if (sortedElements.Contains(Elements.None))
                return new Poof();

            return _elementsToSpell[sortedElements];
        }
    }
}
