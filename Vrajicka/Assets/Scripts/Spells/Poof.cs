using UnityEngine;

namespace Spells
{
    public class Poof : Spell
    {
        public override void Cast(Vector3 initialPosition, Quaternion initialRotation)
        {
            // destroy any other 'poofs'
            var other = GameObject.Find("Poof(Clone)");
            if (other != null)
                Object.Destroy(other);

            this.SpellObject = Object.Instantiate(this.Prefab, initialPosition + Vector3.up * 2, initialRotation);
            this.SpellObject.GetComponent<Rigidbody>().AddForce(Vector3.up * 100);
            Object.Destroy(this.SpellObject, 1);
        }
    }
}
