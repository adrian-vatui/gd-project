using UnityEngine;

namespace Spells
{
    public class Waterball : Spell
    {
        public override void Cast(Vector3 initialPosition, Quaternion initialRotation)
        {
            SpellObject = Object.Instantiate(Prefab, new Vector3(initialPosition.x, 0, initialPosition.z),
                initialRotation);
        }
    }
}
