using System;
using Unity.VisualScripting;
using UnityEngine;
using Utils;

namespace Spells
{
    public class MudBehaviour : MonoBehaviour
    {
        [SerializeField] private float destroyAfter = 5;

        void Start()
        {
            Destroy(gameObject, destroyAfter);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Enemy") && other.GetComponent<Slow>() == null)
                other.AddComponent<Slow>();
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject.CompareTag("Enemy") && other.GetComponent<Slow>() == null)
                other.AddComponent<Slow>();
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag("Enemy") && other.GetComponent<Slow>() != null)
                Destroy(other.GetComponent<Slow>());
        }
    }
}
