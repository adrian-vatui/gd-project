using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DG.Tweening;
using Unity.VisualScripting;
using Sequence = DG.Tweening.Sequence;

namespace Spells
{
    public class WaterballBehaviour : MonoBehaviour
    {
        [SerializeField] private int damage = 20;
        [SerializeField] private float healRatio = 0.1f;
        [SerializeField] private float knockBackForce = 30;
        [SerializeField] private float duration = 1;
        [SerializeField] private float maxScale = 7;

        private HashSet<GameObject> _hit;
        private HealthBehaviour _playerHealth;

        private void Awake()
        {
            _hit = new HashSet<GameObject>();
            _playerHealth = GameObject.Find("Player").GetComponent<HealthBehaviour>();
            ScaleUp();
        }

        private void ScaleUp()
        {
            Sequence seq = DOTween.Sequence();
            seq.Append(transform.DOScale(new Vector3(maxScale, maxScale, maxScale), duration));
            seq.AppendCallback(() => Destroy(gameObject));
        }

        private void OnTriggerEnter(Collider other)
        {
            if (_hit.Contains(other.gameObject) || !other.gameObject.CompareTag("Enemy")) return;

            _hit.Add(other.gameObject);
            var health = other.gameObject.GetComponentInParent<HealthBehaviour>();
            health.TakeDamage(new Damage(damage, Elements.Water));

            // heal for at least 1
            _playerHealth.Heal((int) (healRatio * damage + 0.5f));

            if (health.currentHealth > 0) return;

            Destroy(other.gameObject.GetComponentInParent<StateMachine>());
            var enemyRigidbody = other.gameObject.GetComponentInParent<Rigidbody>();
            enemyRigidbody.isKinematic = false;
            enemyRigidbody.AddExplosionForce(knockBackForce, gameObject.transform.position, 0, 0, ForceMode.Impulse);
        }
    }
}
