using Unity.VisualScripting;
using UnityEngine;

namespace Spells
{
    public class FireballBehaviour : MonoBehaviour
    {
        [SerializeField] private int damage = 60;
        [SerializeField] private float radius = 10;
        [SerializeField] private float knockBackForce = 03;
        private bool _isColliding;

        public void OnCollisionEnter(Collision collision)
        {
            if (_isColliding) return;
            if (collision.gameObject.CompareTag("Player")) return;
            _isColliding = true;

            // Debug.Log($"Hit {collision.gameObject.name}");

            Collider[] colliders = Physics.OverlapSphere(collision.transform.position, radius);

            

            foreach (var col in colliders)
            {
                if (!col.gameObject.CompareTag("Enemy")) continue;

                
                var health = col.gameObject.GetComponentInParent<HealthBehaviour>();
                health.TakeDamage(new Damage(damage, Elements.Fire));

                if (health.currentHealth > 0) continue;

                Destroy(col.gameObject.GetComponentInParent<StateMachine>());
                var enemyRigidbody = col.gameObject.GetComponentInParent<Rigidbody>();
                if (enemyRigidbody is null) continue;
                enemyRigidbody.isKinematic = false;
                enemyRigidbody.AddExplosionForce(knockBackForce, collision.transform.position, 0, 0, ForceMode.Impulse);
            }

            Debug.Log("ajung aici");
            Destroy(gameObject);
            // spawn particle effects etc
        }

        private void Update()
        {
            _isColliding = false;
        }
    }
}
