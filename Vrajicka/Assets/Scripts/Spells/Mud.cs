using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Spells
{
    public class Mud : Spell
    {
        public override void Cast(Vector3 initialPosition, Quaternion initialRotation)
        {
            var actions = GameObject.Find("Player").GetComponent<Movement>().Actions;
            int layerMask = LayerMask.GetMask("Floor");
            Ray ray = Camera.main!.ScreenPointToRay(actions.Moving.MousePosition.ReadValue<Vector2>());

            if (!Physics.Raycast(ray, out var hit, 100000f, layerMask)) return;

            // destroy other mud pool
            var other = GameObject.Find("Mud(Clone)");
            if (other != null)
                Object.Destroy(other);
            SpellObject = Object.Instantiate(Prefab, new Vector3(hit.point.x, 0.5f, hit.point.z),
                initialRotation);
        }
    }
}
