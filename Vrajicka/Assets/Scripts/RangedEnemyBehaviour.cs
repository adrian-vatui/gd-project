using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemyBehaviour : MonoBehaviour
{
    [SerializeField]
    private Animator animator = null;

    [SerializeField]
    private GameObject throwablePrefab;

    public int damageAmmount;
    public Elements element;

    private GameObject playerObject = null;

    private Transform positionToSpawnBall;

    private void Awake()
    {
        positionToSpawnBall = transform.Find("SpawningSpot").transform;
    }

    public void TriggerThrowing()
    {
        if (animator is null) animator = GetComponent<Animator>();
        animator.SetTrigger("throwTrigger");
    }

    public void ThrowObjectAtPlayer()
    {
        if (playerObject is null) playerObject = GameObject.FindGameObjectWithTag("Player");

        ThrowObjectAtPlayer(playerObject.transform.position);
    }

    [SerializeField]
    private float forceToThrow = 10f;
    
    public void ThrowObjectAtPlayer(Vector3 playerPosition)
    {
        var objectThrown = Instantiate(throwablePrefab, positionToSpawnBall.position, Quaternion.identity);

        var direction = playerPosition - positionToSpawnBall.position;

        objectThrown.GetComponent<Rigidbody>().AddForce(direction.normalized * forceToThrow);


        var dealDamage = objectThrown.GetComponent<DamageOnContact>();

        if (dealDamage is null) return;

        dealDamage.amountOfDamage = damageAmmount;

        //var arcProj = objectThrown.GetComponent<ArcProjectileBehaviour>();

        //if (arcProj is null) return;

        //arcProj.ThrowAt(playerPosition);
    }

}
