using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointsLoadBalancer : MonoBehaviour
{
    private List<Transform> spawnPoints = new List<Transform>();


    private void Awake()
    {
        LoadSpawnPoints();
    }

    private void LoadSpawnPoints()
    {
        foreach (Transform child in transform)
        {
            spawnPoints.Add(child);
        }
    }

    public Transform GetRandomSpawnPoint()
    {
        int index = Random.Range(0, spawnPoints.Count);
        var s = spawnPoints[index];
        spawnPoints.RemoveAt(index);

        if(spawnPoints.Count == 0)
        {
            LoadSpawnPoints();
        }

        return s;
    }
}
