using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUIManager : MonoBehaviour
{
    [SerializeField] private GameObject canvases;

    public void Quit()
    {
        Application.Quit();
    }

    public void Play()
    {
        SceneManager.LoadScene("Arena");
    }

    public void SwitchMenu(GameObject panelToActivate)
    {
        foreach (Transform panel in canvases.transform)
        {
            panel.gameObject.SetActive(panel.gameObject == panelToActivate);
        }
    }
}
