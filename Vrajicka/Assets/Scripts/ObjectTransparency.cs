using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(MeshRenderer))]
public class ObjectTransparency : MonoBehaviour
{
    private MeshRenderer renderer;


    [SerializeField]
    private float starting_alpha = 0;



    private void Start()
    {
        renderer = GetComponent<MeshRenderer>();
        SetTransparency(starting_alpha);
    }

    private void Update()
    {

    }

    public void SetTransparency(float alpha)
    {
        Color currentColor = renderer.material.color;
        renderer.material.color = new Color(currentColor.a, currentColor.g, currentColor.b, alpha);
    }
}

