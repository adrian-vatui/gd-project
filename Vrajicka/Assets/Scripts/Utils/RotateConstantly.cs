using UnityEngine;

namespace Utils
{
    public class RotateConstantly : MonoBehaviour
    {
        [SerializeField] private float rotateSpeed = 100f;

        // Update is called once per frame
        void Update()
        {
            transform.Rotate(new Vector3(0, rotateSpeed / Time.deltaTime, 0));
        }
    }
}
