using System.Collections;
using UnityEngine;

namespace Utils
{
    public class DamageOverTime : MonoBehaviour
    {
        public float destroyAfter = 3;
        public int damage = 20;
        public float applyEvery = 1;

        private HealthBehaviour _health;

        void Start()
        {
            // TODO add (and remove) burning effect to object this is attached to
            _health = GetComponentInParent<HealthBehaviour>();
            Destroy(this, destroyAfter);
            StartCoroutine(Dot());
        }

        private IEnumerator Dot()
        {
            while (true)
            {
                _health.TakeDamage(new Damage(damage, Elements.Fire));
                yield return new WaitForSeconds(applyEvery);
            }
        }
    }
}
