using System;
using UnityEngine;
using UnityEngine.AI;

namespace Utils
{
    public class Slow : MonoBehaviour
    {
        public float destroyAfter = 0.5f;
        [Range(0, 100)] public float slowAmount = 80;

        private NavMeshAgent _movement;

        void Start()
        {
            // TODO add (and remove) mud covered effect to object this is attached to
            _movement = GetComponentInParent<NavMeshAgent>();
            _movement.speed -= _movement.speed * slowAmount / 100;
            Destroy(this, destroyAfter);
        }

        private void OnDestroy()
        {
            // math magic to restore original walking speed
            _movement.speed = _movement.speed / (1 - slowAmount / 100);
        }
    }
}
