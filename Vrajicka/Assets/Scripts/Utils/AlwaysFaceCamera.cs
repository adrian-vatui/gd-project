using UnityEngine;

namespace Utils
{
    public class AlwaysFaceCamera : MonoBehaviour
    {
        void Update()
        {
            transform.LookAt(Camera.main!.transform);
        }
    }
}
