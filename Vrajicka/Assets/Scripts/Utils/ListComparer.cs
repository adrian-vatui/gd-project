using System.Collections.Generic;
using System.Linq;

namespace Utils
{
    // straight from https://stackoverflow.com/questions/10020541/c-sharp-list-as-dictionary-key
    class ListComparer<T> : IEqualityComparer<List<T>>
    {
        public bool Equals(List<T> x, List<T> y)
        {
            return x.SequenceEqual(y);
        }

        public int GetHashCode(List<T> obj)
        {
            int hashcode = 0;
            foreach (T t in obj)
            {
                hashcode ^= t.GetHashCode();
            }

            return hashcode;
        }
    }
}
