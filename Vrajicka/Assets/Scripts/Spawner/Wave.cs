using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Wave", menuName = "ScriptableObjects/Spawner/Wave", order = 1)]
public class Wave : ScriptableObject
{
    public WaveSlot[] waveSlots;
    public float timeForWave = 30;
}


[System.Serializable]
public class WaveSlot
{

    [System.Serializable]
    public class WaveSlotElement
    {
        public SpawnGroup spawnGroup;
        public int weight;
    }

    public WaveSlotElement[] waveSlotElements;

    private float[] probabilities = null;


    public SpawnGroup GetSpawnGroup()
    {
        if (probabilities is null || (probabilities.Length == 0)) probabilities = calculateProbabilities();

        float r = Random.Range(0, 1);

        for(int i = 0; i < waveSlotElements.Length; ++i)
        {
            if (r < probabilities[i]) 
                return waveSlotElements[i].spawnGroup;
        }


        return waveSlotElements[waveSlotElements.Length - 1].spawnGroup;
    }

    private float[] calculateProbabilities()
    {
        int sum = 0;
        foreach (var waveSlot in waveSlotElements)
        {
            sum += waveSlot.weight;
        }

        float[] probs = new float[waveSlotElements.Length];
        for(int i = 0; i < probs.Length; ++i)
        {
            probs[i] = (float)waveSlotElements[i].weight / sum;
        }
        for(int i = 1; i < probs.Length; ++i)
        {
            probs[i] += probs[i - 1];
        }
        return probs;
    }

}


