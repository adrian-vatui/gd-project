using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    public WaveDescriber[] waves;
    private int _currentWaveIndex = 0;

    public int currentWaveIndex
    {
        get
        {
            return _currentWaveIndex;
        }
        private set
        {
            _currentWaveIndex = value;
        }
    }


    private Wave _currentWave;

    public Wave currentWave
    {
        get
        {
            return _currentWave;
        } 
        private set
        {
            _currentWave = value;
        }
    }

    public delegate void OnNewWave_Handler(int waveIndex, Wave wave);
    public OnNewWave_Handler OnNewWave;

    private bool timerIsOn = false;

    private SpawnPointsLoadBalancer spawnPoints;

    private void Start()
    {
        spawnPoints = GetComponentInChildren<SpawnPointsLoadBalancer>();
        StartWaves();
    }

    private void StartWaves()
    {
        StartCoroutine(WavesBehaviour());
    }

    public delegate void OnWavesEnded_Handler();
    public OnWavesEnded_Handler OnWavesEnded;

    private int _nbEntitiesAlive = 0;

    public int nbEntitiesAlive
    {
        get
        {
            return _nbEntitiesAlive;
        }
        private set
        {
            _nbEntitiesAlive = value;
        }
    }

    private IEnumerator WavesBehaviour()
    {
        for (int i = 0; i < waves.Length; ++i)
        {
            currentWaveIndex = i;
            currentWave = waves[i].GetWave();
            OnNewWave?.Invoke(currentWaveIndex, currentWave);

            nbEntitiesAlive += SpawnEntities(currentWave);
            

            timerIsOn = true;
            var timerCoroutine = StartCoroutine(WaveTimer(currentWave.timeForWave));

            if (i != waves.Length - 1)
                yield return new WaitUntil(() => !timerIsOn || nbEntitiesAlive == 0);
            else
                yield return new WaitUntil(() => nbEntitiesAlive == 0);

            if (timerCoroutine is not null) StopCoroutine(timerCoroutine);

            timerIsOn = false;
        }

        OnWavesEnded?.Invoke();
    }

    private int SpawnEntities(Wave currentWave)
    {
        int nbEntities = 0;

        foreach (var waveSlot in currentWave.waveSlots)
        {
            var spawnerPosition = spawnPoints.GetRandomSpawnPoint();

            var spawnGroup = waveSlot.GetSpawnGroup();

            Transform spawnerTransform = spawnPoints.GetRandomSpawnPoint();

            foreach (var miniGroup in spawnGroup.minigroups)
            {
                if(miniGroup is null)
                {
                    Debug.LogWarning("This minigroup is null, it shouldn't be...");
                }
                int quantity = miniGroup.getQuantity();
               
                for(int i = 0; i < quantity; ++i)
                {
                    var position = GetPositionToSpawn(spawnerTransform);
                    var objectSpawned = Instantiate(miniGroup.spawnable, position, Quaternion.identity);

                    var healthBehaviour = objectSpawned.GetComponent<HealthBehaviour>();
                    if(healthBehaviour is not null)
                    {
                        healthBehaviour.OnHealthReachedZero += DecrementEntities;
                    }

                }

                nbEntities += quantity;
            }

        }

        return nbEntities;
    }

    private void DecrementEntities()
    {
        --nbEntitiesAlive;
    }

    public delegate void OnWaveTimerTick_Handler(float timePassed, float target);
    public OnWaveTimerTick_Handler OnWaveTimerTick;

    public delegate void OnWaveTimerEnded_Handler();
    public OnWaveTimerEnded_Handler OnWaveTimerEnded;
    private IEnumerator WaveTimer(float seconds)
    {
        float timePassed = 0;
        
        while(true)
        {
            yield return null;
            timePassed += Time.deltaTime;
            OnWaveTimerTick?.Invoke(timePassed, seconds);
            if(timePassed > seconds)
                break;
        }

        timerIsOn = false;
        OnWaveTimerEnded?.Invoke();
    }

    private Vector3 GetPositionToSpawn(Transform spawnerTransform)
    {
        Vector3 randomDisplacement = new Vector3(Random.Range(0, 5), 0, Random.Range(0, 5));

        return spawnerTransform.position + randomDisplacement;
    }
}


[System.Serializable]
public class WaveDescriber
{
    [System.Serializable]
    public class PossibleWaves
    {
        public Wave wave;
        public int weight;
    }

    public PossibleWaves[] possibleWaves;

    private float[] probabilities = null;


    public Wave GetWave()
    {
        if (probabilities is null || (probabilities.Length == 0)) probabilities = calculateProbabilities();

        float r = Random.Range(0, 1);

        for (int i = 0; i < possibleWaves.Length; ++i)
        {
            if (r < probabilities[i]) return possibleWaves[i].wave;
        }


        return possibleWaves[possibleWaves.Length - 1].wave;
    }

    private float[] calculateProbabilities()
    {
        int sum = 0;
        foreach (var possibleWave in possibleWaves)
        {
            sum += possibleWave.weight;
        }

        float[] probs = new float[possibleWaves.Length];
        for (int i = 0; i < probs.Length; ++i)
        {
            probs[i] = (float)possibleWaves[i].weight / sum;
        }
        for (int i = 1; i < probs.Length; ++i)
        {
            probs[i] += probs[i - 1];
        }
        return probs;
    }


}
