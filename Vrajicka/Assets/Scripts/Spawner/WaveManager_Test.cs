using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager_Test : MonoBehaviour
{
    private WaveManager waveManager;
    void Start()
    {
        waveManager = gameObject.GetComponent<WaveManager>();
        waveManager.OnNewWave += (index, wave) => Debug.Log($"Suntem la wave-ul {index}");

        waveManager.OnWavesEnded += () => Debug.Log("All waves have ended!");
    }

   
}
