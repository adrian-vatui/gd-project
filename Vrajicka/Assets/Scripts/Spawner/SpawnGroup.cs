using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "SpawnGroup", menuName = "ScriptableObjects/Spawner/SpawnGroup", order = 1)]
public class SpawnGroup : ScriptableObject
{
    public MiniGroup[] minigroups;
}
