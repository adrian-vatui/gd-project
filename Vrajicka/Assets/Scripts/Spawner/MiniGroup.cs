using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "MiniGroup", menuName = "ScriptableObjects/Spawner/MiniGroup", order = 1)]
public class MiniGroup : ScriptableObject
{
    public GameObject spawnable;
    public int lowEnd;
    public int highEnd;

    public int getQuantity()
    {
        return Random.Range(lowEnd, highEnd + 1);
    }
}
