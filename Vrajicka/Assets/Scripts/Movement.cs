using System;
using UnityEngine;


[RequireComponent(typeof(CharacterController))]
public class Movement : MonoBehaviour
{
    public PlayerActions Actions;
    public float walkingSpeed = 0.2f;
    private float _walkingSpeed;

    public float dashDistance = 10;


    public GameObject indicatorPrefab;
    private GameObject _indicator;

    private CharacterController _controller;

    private bool _dashing = false;

    // Start is called before the first frame update
    private void Awake()
    {
        _walkingSpeed = walkingSpeed;
        Actions = new PlayerActions();
        _controller = GetComponent<CharacterController>();

        Actions.Moving.SetMoveTarget.performed += ctx => SetIndicator();
    }

    private void SetIndicator()
    {
        // if player is dashing, don't allow position changing
        if (_dashing)
            return;

        if (_indicator == null)
            _indicator = Instantiate(indicatorPrefab);


        int layerMask = LayerMask.GetMask("Floor");
        RaycastHit hit;
        Ray ray = Camera.main!.ScreenPointToRay(Actions.Moving.MousePosition.ReadValue<Vector2>());

        if (Physics.Raycast(ray, out hit, 100000f, layerMask))
            _indicator.transform.position = hit.point;
    }

    public void Dash()
    {
        _dashing = true;
        gameObject.layer = LayerMask.NameToLayer("Ghost");

        if (_indicator == null)
            _indicator = Instantiate(indicatorPrefab);
        else
        {
            // reset indicator position under player to ensure the dash starts from the player's current location
            _indicator.transform.position =
                new Vector3(transform.position.x, _indicator.transform.position.y, transform.position.z);
        }

        // increase walking speed to "dash"
        _walkingSpeed *= 10;

        int layerMask = LayerMask.GetMask("Floor");
        Ray ray = Camera.main!.ScreenPointToRay(Actions.Moving.MousePosition.ReadValue<Vector2>());

        if (!Physics.Raycast(ray, out var hit, 100000f, layerMask)) return;

        // get direction to dash in
        var direction = (hit.point - _indicator.transform.position);
        direction.y = 0;
        direction = direction.normalized;

        // perform dash
        _indicator.transform.position += direction * dashDistance;
    }

    private void OnEnable()
    {
        Actions.Enable();
    }

    private void OnDisable()
    {
        Actions.Disable();
    }

    public delegate void OnPlayerStartedMoving_Handler();
    public OnPlayerStartedMoving_Handler OnPlayerStartedMoving;

    public delegate void OnPlayerStoppedMoving_Handler();
    public OnPlayerStartedMoving_Handler OnPlayerStoppedMoving;

    bool isWalking = false;
    private void Update()
    {
        if (_indicator == null) return;

        var direction = (_indicator.transform.position - transform.position);

        // if player is on top of the indicator, reset his speed (dash ended)
        direction = new Vector3(direction.x, 0, direction.z);
        if (direction.magnitude <= 0.2f)
        {
            // dash completed
            _walkingSpeed = walkingSpeed;
            _dashing = false;
            
            gameObject.layer = LayerMask.NameToLayer("Player");
            if(isWalking)
            {
                isWalking = false;
                OnPlayerStoppedMoving?.Invoke();
            }
            return;
        }

        if(!isWalking)
        {
            isWalking = true;
            OnPlayerStartedMoving?.Invoke();
        }

        direction = direction.normalized;
        direction.y = 0;

        _controller.Move(direction * (_walkingSpeed * Time.deltaTime));
        transform.rotation = Quaternion.LookRotation(direction);
    }
}
