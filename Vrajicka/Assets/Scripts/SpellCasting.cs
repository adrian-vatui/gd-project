using Spells;
using UnityEngine;

public class SpellCasting : MonoBehaviour
{
    private PlayerActions _actions;

    [SerializeField] private int nbElementsForSpell = 2;

    private int _currentIndex = 0;
    private Elements[] _currentElements;

    private void Awake()
    {
        _currentElements = new Elements[nbElementsForSpell];
        _currentIndex = 0;
        for (int i = 0; i < _currentElements.Length; ++i)
        {
            _currentElements[i] = Elements.None;
        }

        _actions = new PlayerActions();

        _actions.SpellCasting.FirstElement.performed += _ => AddElement(Elements.Fire);
        _actions.SpellCasting.SecondElement.performed += _ => AddElement(Elements.Water);
        _actions.SpellCasting.ThirdElement.performed += _ => AddElement(Elements.Earth);


        _actions.SpellCasting.ClearElements.performed += _ => ClearElements();
        _actions.SpellCasting.CastSpell.performed += _ => CastSpell();


        //test only not for production pls
        // OnElementAdded += (elements, _) => Debug.Log($"{elements[0]},{elements[1]}");
    }


    public delegate void OnElementAddedHandler(Elements[] currentElements, Elements elementAdded);

    public OnElementAddedHandler OnElementAdded;

    private void AddElement(Elements element)
    {
        if (_currentElements[_currentIndex] == element)
        {
            int oldIndex = _currentIndex;
            _currentIndex = (_currentIndex + 1) % nbElementsForSpell;
            bool foundDifferentElement = false;
            while (_currentIndex != oldIndex)
            {
                if (_currentElements[_currentIndex] != element)
                {
                    foundDifferentElement = true;
                    break;
                }

                _currentIndex = (_currentIndex + 1) % nbElementsForSpell;
            }

            if (!foundDifferentElement)
            {
                return;
            }
        }

        _currentElements[_currentIndex] = element;
        _currentIndex = (_currentIndex + 1) % nbElementsForSpell;

        OnElementAdded?.Invoke(_currentElements, element);
    }

    public delegate void OnElementsClearedHandler();

    public OnElementsClearedHandler OnElementsCleared;

    private void ClearElements()
    {
        for (int i = 0; i < _currentElements.Length; ++i)
        {
            _currentElements[i] = Elements.None;
        }

        _currentIndex = 0;
        OnElementsCleared?.Invoke();
    }


    private void CastSpell()
    {
        var mousePosition = _actions.Moving.MousePosition.ReadValue<Vector2>();

        Ray ray = Camera.main!.ScreenPointToRay(mousePosition);

        Physics.Raycast(ray, out var hit, 1000f);

        hit.point = new Vector3(hit.point.x, transform.position.y, hit.point.z);

        var rotation = Quaternion.LookRotation(hit.point - transform.position);

        var spell = SpellFactory.CreateSpell(_currentElements);
        spell.Cast(transform.position, rotation);

        ClearElements();
    }

    private void OnEnable()
    {
        _actions.Enable();
    }

    private void OnDisable()
    {
        _actions.Disable();
    }
}
