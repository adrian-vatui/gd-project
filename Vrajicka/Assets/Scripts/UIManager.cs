using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private WaveManager waveManager;
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject deathScreen;
    [SerializeField] private Text waveNumber;
    [SerializeField] private Text waveTimer;

    private Dictionary<Elements, Image> _elementsToUI;

    public void Awake()
    {
        var spellCasting = player.GetComponent<SpellCasting>();
        var playerHealth = player.GetComponent<HealthBehaviour>();

        _elementsToUI = new Dictionary<Elements, Image>();

        foreach (int i in Enum.GetValues(typeof(Elements)))
        {
            if ((Elements) i == Elements.NbElements)
                break;

            String elementName = Enum.GetName(typeof(Elements), i);
            _elementsToUI.Add((Elements) i,
                transform.Find($"{elementName}Element/{elementName}Highlight").GetComponent<Image>());
        }

        spellCasting.OnElementAdded += (elements, _) => UpdateSelectedElements(elements);
        spellCasting.OnElementsCleared += DeselectElements;
        playerHealth.OnHealthReachedZero += GameOver;

        waveManager.OnNewWave += UpdateWaveNumber;
        waveManager.OnWaveTimerTick += UpdateWaveTimer;
    }

    private void UpdateSelectedElements(Elements[] elements)
    {
        DeselectElements();

        foreach (var element in elements)
            if (element != Elements.None)
                _elementsToUI[element].gameObject.SetActive(true);
    }

    private void DeselectElements()
    {
        foreach (var highlight in _elementsToUI.Values)
            highlight.gameObject.SetActive(false);
    }

    public void Retry()
    {
        SceneManager.LoadScene("Arena");
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    private void UpdateWaveNumber(int waveIndex, Wave wave)
    {
        waveNumber.text = "Wave:" + (waveIndex + 1);
    }

    private void UpdateWaveTimer(float timePassed, float target)
    {
        waveTimer.text = "Next wave in: " + Math.Round(target - timePassed);
    }

    private void GameOver()
    {
        if (deathScreen.activeSelf) return;

        deathScreen.SetActive(true);
        int i = PlayerPrefs.GetInt("index", 0);
        PlayerPrefs.SetInt("HighestWave" + i, waveManager.currentWaveIndex + 1);
        PlayerPrefs.SetString("DateTime" + i, DateTime.Now.ToString());
        PlayerPrefs.SetInt("Time" + i, (int) Math.Round(Time.timeSinceLevelLoad));
        PlayerPrefs.SetInt("index", i + 1);
        PlayerPrefs.Save();
    }
}
