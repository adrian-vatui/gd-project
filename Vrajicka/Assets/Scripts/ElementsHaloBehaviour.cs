using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementsHaloBehaviour : MonoBehaviour
{

    ElementalOrbBehaviour[] orbs;


    void Start()
    {

        var spellCasting = FindObjectOfType<SpellCasting>();
        if (spellCasting == null) return;

       
        spellCasting.OnElementAdded += SetHalo;

        spellCasting.OnElementsCleared += ClearHalo;

        orbs = new ElementalOrbBehaviour[transform.childCount];
        for(int i = 0; i < transform.childCount; ++i)
        {
            orbs[i] = transform.GetChild(i).gameObject.GetComponent<ElementalOrbBehaviour>();
        }
    }

    private void SetHalo(Elements[] elements, Elements elementAdded)
    {
        int nbElements = 0;
        for(int i = 0; i < elements.Length; ++i)
        {
            if (elements[i] == Elements.None) break;
            nbElements++;
        }
        if (nbElements == 0) return;

        float deltaAngle = 360.0f / nbElements;
        for(int i = 0; i < nbElements; ++i)
        {
            orbs[i].gameObject.SetActive(true);
            orbs[i].SetCorrectElement(elements[i]);

            orbs[i].transform.localRotation = Quaternion.Euler(0, i * deltaAngle, 0);
        }
    }

    private void ClearHalo()
    {
        for(int i = 0; i < orbs.Length; ++i)
        {
            orbs[i].gameObject.SetActive(false);
        }
       
    }
}
