using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class HealthUI : MonoBehaviour
{
    private Transform _redBar;
    private HealthBehaviour _health;

    private void Awake()
    {
        _health = transform.GetComponentInParent<HealthBehaviour>();
        _redBar = transform.GetChild(0); // red bar should always be on top
    }

    

    private void Update()
    {
        transform.LookAt(Camera.main!.transform, Vector3.down);
        float scale = (float) _health.currentHealth / (float) _health.maximumHealth;
        _redBar.localScale = new Vector3(scale, _redBar.localScale.y, _redBar.localScale.z);
    }
}
