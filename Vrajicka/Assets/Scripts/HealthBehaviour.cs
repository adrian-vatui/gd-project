using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBehaviour : MonoBehaviour
{
    #region HealthRelated

    [Header("Health")]
    [SerializeField]
    private int _maximumHealth = 100;

    public int maximumHealth
    {
        get
        {
            return _maximumHealth;
        }
    }


    [SerializeField]
    [Tooltip("Made this visible in the inspector just for it to be easier to read, DON'T change it in the inspector")]
    private int _currentHealth = 0;

    public delegate void OnHealthReachedZero_Handler();
    public OnHealthReachedZero_Handler OnHealthReachedZero;

    public int currentHealth {
        get
        {
            return _currentHealth;
        }
        private set
        {
            int amount = Mathf.Min(_maximumHealth, value);
            int difference = amount - _currentHealth;
            _currentHealth = amount;

            OnHealthChanged?.Invoke(currentHealth, difference);

            if (_currentHealth == 0) OnHealthReachedZero?.Invoke();
        }
    }


    public delegate void OnHealthSetToMaximum_Handler(int currentHealth);
    public OnHealthSetToMaximum_Handler OnHealthSetToMaximum;

    public delegate void OnHealthChanged_Handler(int currentHealth, int amount);
    public OnHealthChanged_Handler OnHealthChanged;
    #endregion

    #region Protections
    [Header("Protections")]

    [SerializeField]
    
    [Tooltip("General protections, this will reduce ALL types of damage (damage - protections)")]
    private int _protections;

    public int protections
    {
        get
        {
            return _protections;
        }
        set
        {
            _protections = value;
        }
    }


    [Header("Elemental protections")]
    [SerializeField]
    [Tooltip("Protections from Fire element")]
    private int _fireProtections;

    public int fireProtections
    {
        get
        {
            return elementalProtections[Elements.Fire];
        }
        set
        {
            elementalProtections[Elements.Fire] = Mathf.Max(value, 0);
        }
    }

    [SerializeField]
    [Tooltip("Protections from Water element")]
    private int _waterProtections;

    public int waterProtections
    {
        get
        {
            return elementalProtections[Elements.Water];
        }
        set
        {
            elementalProtections[Elements.Water] = Mathf.Max(value, 0);
        }
    }

    [SerializeField]
    [Tooltip("Protections from Earth element")]
    private int _earthProtections;

    public int earthProtections
    {
        get
        {
            return elementalProtections[Elements.Earth];
        }
        set
        {
            elementalProtections[Elements.Earth] = Mathf.Max(value, 0);
        }
    }
    #endregion

    #region Resistances 
    [Header("Resistances")]

    [SerializeField]

    [Tooltip("Damage resistance, E.g: if damageResistance = 0.2, that means it will ignore 0.2 of damage taken (after protections)")]
    [Range(0.0f, 1.0f)]
    private float _damageResistance;

    public float damageResistance
    {
        get
        {
            return _damageResistance;
        }
    }


    [Header("Elemental resistances")]
    [SerializeField]
    [Tooltip("Damage resistance from Fire element")]
    [Range(0.0f, 1.0f)]
    private float _fireResistance;

    public float fireResistance
    {
        get
        {
            return elementalResistances[Elements.Fire];
        }
        set
        {
            float amount = Mathf.Max(Mathf.Min(value, 1), 0);
            elementalResistances[Elements.Fire] = amount;
        }
    }

    [SerializeField]
    [Tooltip("Damage resistance from Water element")]
    [Range(0.0f, 1.0f)]
    private float _waterResistance;

    public float waterResistance
    {
        get
        {
            return elementalResistances[Elements.Water];
        }
        set
        {
            float amount = Mathf.Max(Mathf.Min(value, 1), 0);
            elementalResistances[Elements.Water] = amount;
        }
    }

    [SerializeField]
    [Tooltip("Damage resistance from Earth element")]
    [Range(0.0f, 1.0f)]
    private float _earthResistance;

    public float earthResistance
    {
        get
        {
            return elementalResistances[Elements.Earth];
        }
        set
        {
            float amount = Mathf.Max(Mathf.Min(value, 1), 0);
            elementalResistances[Elements.Earth] = amount;
        }
    }


    #endregion

    private Dictionary<Elements, int> elementalProtections = new Dictionary<Elements, int>();

    private Dictionary<Elements, float> elementalResistances = new Dictionary<Elements, float>();
    private void Awake()
    {
        elementalProtections[Elements.None] = 0;
        elementalResistances[Elements.None] = 0;


        //adding the elemental protections to this dictionary in order to be easier access
        elementalProtections[Elements.Fire] = _fireProtections;
        elementalProtections[Elements.Water] = _waterProtections;
        elementalProtections[Elements.Earth] = _earthProtections;


        elementalResistances[Elements.Fire] = _fireResistance;
        elementalResistances[Elements.Water] = _waterResistance;
        elementalResistances[Elements.Earth] = _earthResistance;

        SetToMaximum();
    }

    public void SetToMaximum()
    { 
        currentHealth = _maximumHealth;

        OnHealthSetToMaximum?.Invoke(currentHealth); 
    }

    /// <summary>
    /// Function called to... do damage. More info in the <see cref="Damage"/> class
    /// It uses all of it's parameters in order to calculate the damage dealt and actually do it.
    /// The order for damage reduction is: First the protections, then the resistances
    /// </summary>
    /// <param name="damage">The actual bread and butter of this function</param>
    public void TakeDamage(Damage damage)
    {
        int amount = damage.amount;
        //calculating the protections
        if(!damage.trueDamage)
        {
            if(!damage.ignoresArmour) 
            { 
                int totalProtections = elementalProtections[damage.element] + protections;
            
                totalProtections -= damage.armourPierceAmount;

                totalProtections = Mathf.Max(totalProtections, 0);

                amount -= totalProtections;
            }

            float totalResistances = elementalResistances[damage.element] + damageResistance;

            amount = Mathf.FloorToInt(amount * (1 - totalResistances));
        }

        //i think letting it be at least 1 damage it's fair
        amount = Mathf.Max(amount, 1);
        currentHealth = Mathf.Max(0, currentHealth - amount);
    }
    
    public void Heal(int healAmount)
    {
        currentHealth = Math.Min(currentHealth + healAmount, maximumHealth);
    }


    /// <summary>
    /// This functions is called whenever a variable is modified inside the inspector
    /// Only for testing purposes
    /// </summary>
    private void OnValidate()
    {
        elementalProtections[Elements.Fire] = _fireProtections;
        elementalProtections[Elements.Water] = _waterProtections;
        elementalProtections[Elements.Earth] = _earthProtections;


        elementalResistances[Elements.Fire] = _fireResistance;
        elementalResistances[Elements.Water] = _waterResistance;
        elementalResistances[Elements.Earth] = _earthResistance;
    }
}


#region DamageClass


/// <summary>
/// The damage package used to transmit more data than just the amount of damage done
/// </summary>
public class Damage
{
    int _amount;
    Elements _element;
    bool _trueDamage;
    //adding this field for damage over time damage
    bool _ignoresArmour;
    int _armourPierceAmount;


    /// <summary>
    ///  
    /// </summary>
    /// <param name="amount">The amount of damage dealt... yeah</param>
    /// <param name="element"> What type of element is this damage from?</param>
    /// <param name="armourPierceAmount"> The amount of protections this damage should ignore </param>
    /// <param name="ignoresArmour"> If this damage should ignore ALL armour </param>
    /// <param name="trueDamage"> If this damage should ignore ALL armour and resistances </param>
    public Damage(int amount, Elements element = Elements.None, int armourPierceAmount = 0, bool ignoresArmour = false, bool trueDamage = false)
    {
        this._amount = amount;
        this._element = element;
        this._armourPierceAmount = armourPierceAmount;
        this._ignoresArmour = ignoresArmour;
        this._trueDamage = trueDamage;
    }


    #region Getters 
    public int amount
    {
        get
        {
            return _amount;
        }
    }

    public Elements element
    {
        get
        {
            return _element;
        }
    }

    public bool trueDamage
    {
        get
        {
            return _trueDamage;
        }
    }

    public int armourPierceAmount
    {
        get
        {
            return _armourPierceAmount;
        }
    }

    public bool ignoresArmour
    {
        get
        {
            return _ignoresArmour;
        }
    }

    #endregion

}

#endregion