using UnityEngine;

[RequireComponent(typeof(HealthBehaviour))]
public class DisappearOnDeath : MonoBehaviour
{
    [SerializeField] private float destroyAfter = 0.5f;

    private void Awake()
    {
        var health = GetComponent<HealthBehaviour>();
        health.OnHealthReachedZero += () => Destroy(gameObject, destroyAfter);
    }
}
