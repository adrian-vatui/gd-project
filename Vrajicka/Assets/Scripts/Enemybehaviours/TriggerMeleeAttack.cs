using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerMeleeAttack : MonoBehaviour
{
    private MeleeEnemyBehaviour enemy;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Player") return;

        if (enemy is null) enemy = GetComponentInParent<MeleeEnemyBehaviour>();

        enemy.PlayerInRange = true;
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag != "Player") return;

        if (enemy is null) enemy = GetComponentInParent<MeleeEnemyBehaviour>();

        enemy.PlayerInRange = false;
    }
}
