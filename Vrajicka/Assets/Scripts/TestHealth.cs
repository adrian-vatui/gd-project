using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestHealth : MonoBehaviour
{
    HealthBehaviour test;

    public int damageToTake = 100;
    public Elements element = Elements.Fire;
    public int armourPierceAmount = 0;
    public bool ignoresArmour = false;
    public bool trueDamage = false;
    void Start()
    {
        actions = new PlayerActions();
        actions.SpellCasting.CastSpell.performed += ctx => DealAndRestoreDamage();
        actions.Enable();
        test = GameObject.Find("Player").GetComponent<HealthBehaviour>();
        test.OnHealthChanged += (currentHealth, amount) => {
            if (amount < 0)
                Debug.Log($"Current Health: {currentHealth} Amount changed: {amount}");
        };
        test.OnHealthReachedZero += () => Debug.Log("He Died :(");
        test.OnHealthSetToMaximum += (currentHealth) => Debug.Log($"Health set to maximum! {currentHealth}");
    }

    private void DealAndRestoreDamage()
    {
        //there are a lot of optional parameters (armourPierce, ignores and trueDamage are all optional)
        //calling them all here just for testing purposes
        test.TakeDamage(new Damage(damageToTake, Elements.Fire, armourPierceAmount, ignoresArmour, trueDamage));
        test.SetToMaximum();
    }

    //doing this only for testing purposes
    PlayerActions actions;
}
