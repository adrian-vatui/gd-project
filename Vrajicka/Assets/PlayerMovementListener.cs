using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementListener : MonoBehaviour
{
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();

        var movement = GetComponentInParent<Movement>();
        movement.OnPlayerStartedMoving += () => animator.SetTrigger("startedWalking");
        movement.OnPlayerStoppedMoving += () => animator.SetTrigger("stoppedWalking");
    }

    
}
