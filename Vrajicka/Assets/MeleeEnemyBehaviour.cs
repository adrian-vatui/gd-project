using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MeleeEnemyBehaviour : MonoBehaviour
{
    public MeshRenderer damageConeRenderer;
    public ObjectTransparency damageConeTransparency;

    public GameObject sword;

    [SerializeField]
    private float telegraphTime = 0.5f;


    [SerializeField]
    private float swingTime = 0.5f;

    [SerializeField]
    private float timeBetweenWarningAndSwing = 0.1f;

    public bool PlayerInRange = false;
    private void Start()
    {
        
    }


    public bool inSwingingAnimation = false;



    public void SwingSword()
    {
        Sequence swingingSequence = DOTween.Sequence();

        swingingSequence.AppendCallback(() => inSwingingAnimation = true);

        swingingSequence.Append(DOTween.To(() => damageConeRenderer.material.color.a, damageConeTransparency.SetTransparency, 0.65f, telegraphTime));
        swingingSequence.AppendInterval(0.01f);
        swingingSequence.Append(DOTween.To(() => damageConeRenderer.material.color.a, damageConeTransparency.SetTransparency, 0.0f, 0.01f));
        swingingSequence.AppendInterval(timeBetweenWarningAndSwing);

        swingingSequence.AppendCallback(() => sword.transform.GetChild(0).gameObject.SetActive(true));
        swingingSequence.Append(sword.transform.DOLocalRotate(new Vector3(0, -50, 0), swingTime));


        swingingSequence.AppendCallback(() => {
            sword.transform.localRotation = Quaternion.Euler(0, 50, 0);
            sword.transform.GetChild(0).gameObject.SetActive(false);
        });


        swingingSequence.AppendCallback(() => inSwingingAnimation = false);
    }
}
