using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;


[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(NavMeshAgent))]
public class EnemyMovementBehaviour : MonoBehaviour
{
    public float walkingSpeed = 10;
    private CharacterController controller;

    private NavMeshAgent agent;


    private void Awake()
    {
        controller = GetComponent<CharacterController>();
        agent = agent = GetComponent<NavMeshAgent>();
        agent.speed = walkingSpeed;
    }

    public void MoveTowards(Vector3 target)
    {
        agent.destination = target;
        //Vector3 directionVector = target - transform.position;
        ////directionVector.y = 0;
        //SmoothRotate(directionVector.normalized);
        //controller.Move(directionVector.normalized * Time.deltaTime * walkingSpeed);
    }


    public void StopMoving()
    {
        if(agent is null) agent = agent = GetComponent<NavMeshAgent>();
        agent.enabled = false;
        Debug.Log("Stopped!");
    }

    public void StartMoving()
    {
        if(agent is null) agent = GetComponent<NavMeshAgent>();
        agent.enabled = true;
        Debug.Log("StartedAgain");
    }

    [SerializeField]
    private float rotationDuration = 0.3f;

    public void SmoothRotate(Vector3 newForwardDirection)
    {
        transform.DOLookAt(transform.position + newForwardDirection * 10, rotationDuration);
    }
}
