# Untitled GD Project

## Team composition

Bujeniță Claudiu Dumitrel, Munteanu Maria, Opriță Ștefan, Popescu Adana, Vătui Adrian

## Type of game

Isometric hack'n'slash

## Core gameplay

You will play as a wizard that can control a few basic elements (water, fire etc.) and cast spells by combining them together (eg. water + fire = steam). The game will take place inside an arena and the player character will face endless hordes of enemies that ramp up in difficulty over time. Thus, the core gameplay loop is experimenting with different combinations to cast various spells to kill enemies as efficiently as possible.

## Links to similar concepts/inspirations:

- [Magicka](https://store.steampowered.com/app/42910/Magicka/)
- [Neophyte](https://regalpigeon.itch.io/neophyte)
- Dota’s overly complicated hero, [Invoker](https://dota2.fandom.com/wiki/Invoker)

## Asset credits
- [Spell UI sprites](https://itch.io/profile/pimen)
